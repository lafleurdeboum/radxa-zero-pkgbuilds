echo "U-boot default fdtfile: ${fdtfile}"
echo "Current variant: ${variant}"

part uuid ${devtype} ${devnum}:${bootpart} uuid
setenv bootargs console=${console} root=PARTUUID=${uuid} rw rootwait
setenv bootargs initrd=/boot/initramfs-linux.img rw console=ttyAML0,115200n8 console=tty0 no_console_suspend consoleblank=0 fsck.fix=yes fsck.repair=yes net.ifnames=0 quiet

echo "trying to import environment from boot/uEnv.txt - I need a 'bootargs' parameter"
if test -e ${devtype} ${devnum} ${prefix}uEnv.txt; then
    load ${devtype} ${devnum} ${scriptaddr} ${prefix}uEnv.txt
    env import -t ${scriptaddr} ${filesize}
fi

if test "${rootlabel}" != ""; then
    echo "adding root label ${rootlabel} to bootargs"
    setenv bootargs "root=LABEL=${rootlabel} ${bootargs}"
else
    if test "${rootuuid}" != ""; then
        echo "adding root uuid ${rootuuid} to bootargs"
        setenv bootargs "root=UUID=${rootuuid} ${bootargs}"
    else
        echo "no rootlabel or rootuuid found in boot/uEnv.txt"
        exit
    fi
fi

for overlay_file in ${overlays}; do
    if load ${devtype} ${devnum} ${scriptaddr} ${prefix}dtb/amlogic/overlay/${overlay_prefix}-${overlay_file}.dtbo; then
        echo "Applying kernel provided DT overlay ${overlay_prefix}-${overlay_file}.dtbo"
        fdt apply ${scriptaddr} || setenv overlay_error "true"
    fi
done

for overlay_file in ${user_overlays}; do
    if load ${devtype} ${devnum} ${scriptaddr} ${prefix}overlay-user/${overlay_file}.dtbo; then
        echo "Applying user provided DT overlay ${overlay_file}.dtbo"
        fdt apply ${scriptaddr} || setenv overlay_error "true"
    fi
done

if test "${overlay_error}" = "true"; then
    echo "Error applying DT overlays, restoring original DT"
    load ${devtype} ${devnum} ${fdt_addr_r} ${prefix}dtb/${fdtfile}
else
    if load ${devtype} ${devnum} ${scriptaddr} ${prefix}dtb/amlogic/overlay/${overlay_prefix}-fixup.scr; then
        echo "Applying kernel provided DT fixup script (${overlay_prefix}-fixup.scr)"
        source ${scriptaddr}
    fi
    if test -e ${devtype} ${devnum} ${prefix}fixup.scr; then
        load ${devtype} ${devnum} ${scriptaddr} ${prefix}fixup.scr
        echo "Applying user provided fixup script (fixup.scr)"
        source ${scriptaddr}
    fi
fi

if load ${devtype} ${devnum}:${bootpart} ${kernel_addr_r} /boot/Image; then
  if load ${devtype} ${devnum}:${bootpart} ${fdt_addr_r} /boot/dtbs/${fdtfile}; then
    if load ${devtype} ${devnum}:${bootpart} ${ramdisk_addr_r} /boot/initramfs-linux.img; then
      booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r};
    else
      booti ${kernel_addr_r} - ${fdt_addr_r};
    fi;
  fi;
fi

