# PKGBUILDs for ArchLinuxArm on Radxa Zero
This is an effort to run the generic aarch64 linux from [archlinuxarm.org](https://archlinuxarm.org) on the [radxa Zero](https://wiki.radxa.com/Zero).

## Installing ArchlinuxARM on a radxa zero

### Partitioning
Get into USB BOOT mode following the Radxa [maskrom] guide. Find out which device
node shows up with `lsblk`. Edit that device with

    # fdisk /dev/sdB

At the prompt, press `o` to create a new DOS partition table. Then press `n` to
create a new partition, enter to make it a primary partition, enter again to
make it partition number one, then `65536` to make it start after a 32MB offset
(reserved for U-Boot), and enter again to let it fill the drive. Press `p` to
see the new partition scheme. Finally press `w` to write the new partition
table to disk, and `q` to quit.

Format the partition as ext4 and label it :

    # mkfs.ext4 -O ^metadata_csum,^64bit /dev/sdB1
    # e2label /dev/sdB1 anArchArm

### Chroot setup
Mount the newly created partition and untar the contents of the [generic
aarch64 ArchLinuxArm] into it :

    # mount /dev/sdB1 /mnt
    # bsdtar xpf ArchLinuxARM-aarch64-latest.tar.gz -C /mnt

Chroot into it and initialize pacman :

    # arch-chroot /mnt
    # pacman-key --init
    # pacman-key --populate

Update the system and install needed dependencies for makepkg :

    # pacman -Syu
    # pacman -S fakeroot sudo

Create a regular user, add it to wheel and clone this repo :

    # adduser MYLOGIN
    # [add it to wheel]
    # su MYLOGIN
    $ git clone https://git.presentmatter.one/lafleur/radxa_zero_pkgbuilds

### Boot setup - `uboot`
_While chrooted_, install the `uboot-radxa-zero-bin` package - this will flash
U-Boot from inside the chrooted-in drive, and create
`/boot/extlinux/extlinux.conf` that is read by U-Boot at startup :

    $ cd radxa_zero_pkgbuilds/uboot-radxa-zero-bin
    $ makepkg -si

Make sure you have a kernel installed at `/boot/Image`. If not, install
`linux-aarch64`. It works fine, but the device-tree overlays are a bit broken.
I'm working on a PKGBUILD for the Radxa kernel, I'll update here.

### Finishing and boot
To resolve URLs with systemd-resolved, remove `/etc/resolv.conf` and do
```
# ln -s /run/systemd/resolve/stub-resolv.conf
```

(Optionally) install the `radxa-zero-wifi` package to get a working wifi :

    $ cd ../../radxa_zero_pkgbuilds/radxa-zero-wifi
    $ makepkg -si

Also edit `/etc/systemd/network/wlan.network` to add LLMNR on this device. It's
an alternative to Bonjour/mDNS that provides hostname resolution on the local
domain. See `man systemd.network` for details.

While you're still chrooted, you may also set various things up :

  - use `hostnamectl` to set the hostname up
  - copy your authorized key to

    `~/.ssh/authorized_keys`

  - To have ethernet emulation on the USB2 socket :
```
# cat > /etc/modules-load.d/usbeth.conf << EOF
dwc2
g_ether
EOF
```

  - To have the system connect to wifi hotspot at boot time, install
    `wpa_supplicant` and run
```
# wpa_passphrase YOUR_SSID ITS_PASSWORD >> /etc/wpa_supplicant-wlan0.conf
# systemctl enable wpa_supplicant@wlan0.service
```
Get out of the chroot, eject the device, apply power to boot. If the hotspot
lets you, you should be able to login with ssh with the hostname you gave it.


[maskrom]: https://wiki.radxa.com/Zero/dev/maskrom#Enable_maskrom
[generic aarch64 ArchLinuxArm]: http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz

